package net.seliba.tablist.listener;

import net.minecraft.server.v1_13_R2.IChatBaseComponent;
import net.minecraft.server.v1_13_R2.PacketPlayOutPlayerListHeaderFooter;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        sendTablist(event.getPlayer(), "§8-----------------------------------------------\n" +
                "§6plagindefs §7| §6Teamserver\n\n" +
                "§aHerzlich Willkommen!",
                "§6Online: §a" + Bukkit.getOnlinePlayers().size() + " §7/ §a" + Bukkit.getMaxPlayers() + "\n" +
                "§6Uhrzeit: §a" + getTime() + "\n" +
                "§8-----------------------------------------------");
    }

    public void sendTablist(Player player, String header, String footer) {
        PacketPlayOutPlayerListHeaderFooter packetPlayOutPlayerListHeaderFooter = new PacketPlayOutPlayerListHeaderFooter();
        packetPlayOutPlayerListHeaderFooter.header = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + header + "\"}");
        packetPlayOutPlayerListHeaderFooter.footer = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + footer + "\"}");
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetPlayOutPlayerListHeaderFooter);
    }

    public  String getTime() {
        final SimpleDateFormat date = new SimpleDateFormat("HH:mm:ss");
        return date.format(Calendar.getInstance().getTime());
    }

}
