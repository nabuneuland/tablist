package net.seliba.tablist;

import net.seliba.tablist.listener.PlayerJoinListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Tablist extends JavaPlugin {

    @Override
    public void onEnable() {
        Bukkit.getConsoleSender().sendMessage("[Tablist] Gestartet!");

        PlayerJoinListener playerJoinListener = new PlayerJoinListener();
        Bukkit.getPluginManager().registerEvents(playerJoinListener, this);

        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, () ->
            Bukkit.getOnlinePlayers().forEach(player -> playerJoinListener.sendTablist(player, "§8-----------------------------------------------\n" +
                            "§6plagindefs §7| §6Teamserver\n\n" +
                            "§aHerzlich Willkommen!\n",
                            "\n§6Online: §a" + Bukkit.getOnlinePlayers().size() + " §7/ §a" + Bukkit.getMaxPlayers() + "\n" +
                            "§6Uhrzeit: §a" + playerJoinListener.getTime() + "\n" +
                            "§8-----------------------------------------------"))
        , 20L, 0L);
    }

}
